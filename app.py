from flask import Flask, request
import os
import requests
import barcode

from barcode.writer import ImageWriter

app = Flask(__name__)


def get_from_env(key):
    """Taking any environmental variable

    Args:
        key: name of env variable

    Returns:
        value of env variable
    """
    return os.environ[key]


def send_message(chat_id, text):
    """Send message with text, in chat with chat_id, using https requests

    Args:
        chat_id: id of chat, where message will be sent
        text: text of message, what will be sent
    """
    method = "sendMessage"
    token = get_from_env("BOT_TOKEN")
    url = f"https://api.telegram.org/bot{token}/{method}"
    data = {"chat_id": chat_id, "text": text}
    requests.post(url, data=data)


def send_barcode(chat_id, ean13):
    """Send message with barcode image in it, in chat with chat_id, using https requests
    Args:
        chat_id: id of chat, where message will be sent
        ean: text of message, what will be sent
    """
    method = "sendPhoto"
    token = get_from_env("BOT_TOKEN")
    url = f"https://api.telegram.org/bot{token}/{method}"
    ean = barcode.get(name='ean13', code=ean13, writer=ImageWriter())
    filename = ean.save('test')
    files = {"photo": open(filename, 'rb')}
    data = {"chat_id": chat_id}
    requests.post(url, files=files, data=data)


@app.route('/', methods=['POST', 'GET'])
def barcode_convert():
    """Function responsible for any message sent to the bot, to the address /.
        If in massage were 13-digits, then convert them into barcode image, and send it back, else send message "Неверный формат ean13"

        Returns:
            {"ok": True}
    """
    try:
        chat_id = request.json["message"]["chat"]["id"]
        try:
            text = request.json["message"]["text"]
            send_barcode(chat_id=chat_id, ean13=text)
        except(barcode.errors.IllegalCharacterError, barcode.errors.NumberOfDigitsError) as ex:
            try:
                send_message(chat_id=chat_id, text=f"Неверный формат ean13")
                print(ex)
            except() as ex:
                print(ex)
                pass
    except:
        pass
    return {"ok": True}


if __name__ == '__main__':
    app.run()
